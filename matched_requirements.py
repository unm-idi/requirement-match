import csv
import json
import hashlib
import pandas as pd
from uuid import uuid4
from utils import get_program_data, course_list_from_requirement
from pydantic import BaseModel, validator, ValidationError
from typing import List, Optional

class Requirements(BaseModel):
    id: str
    requirement_name: str
    requirement_type: str
    program_code: str
    academic_period: str
    course_list: List[str]
    courses_hash: str
    match_score: float
    name_match: bool
    group_id: str

    @validator("requirement_name", pre=True)
    def none_to_empty(cls, v: object) -> object:
        if v is None:
            return f"Blank Requirement for {cls.program_code}"
        return str(v)
    @validator("match_score", pre=True)
    def round_score(cls, v: object) -> object:
        return round(v, 2)

def similarity_score(len_prime, len_second, len_intersect):
    if len_prime == 0 or len_second == 0:
        return 0
    return ((len_intersect / len_prime) + (len_intersect / len_second)) / 2

def get_score(curr_req, prev_req, intersect_count):
    score = 0.0
    name_match = False
    if curr_req.requirement_name == prev_req.requirement_name:
        if curr_req.courses_hash == prev_req.courses_hash:
            score = 100
        else:  # len(curr_req.course_list) > 0:
            score = similarity_score(len(curr_req.course_list), len(prev_req.course_list), intersect_count)
            score = round(score * 100, 2)
        name_match = True
    elif curr_req.courses_hash == prev_req.courses_hash and len(curr_req.course_list) > 0:
        score = 100
    else:
        score = similarity_score(len(curr_req.course_list), len(prev_req.course_list), intersect_count)
        score = round(score * 100, 2)
    return score, name_match
    
def to_csv(list_object: list, filename: str, mode='w'):
    if list_object:
        with open(filename, mode, newline='') as csvfile:
            # Extract fieldnames from the first entry
            fieldnames = list_object[0].dict().keys()
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            
            writer.writeheader()  # Write the header row
            for req in list_object:
                req_dict = req.dict()
                writer.writerow(req_dict)
        print(f"Requirements have been written to {filename}")

def intersection_count(requirement_1: Requirements, requirement_2: Requirements):
    lu_crs_1 = list()
    lu_crs_2 = list()
    courses_list_1 = requirement_1.course_list
    for course1 in courses_list_1:
        lu_crs_1.append(lookup_course_equivalence(course1))

    courses_list_2 = requirement_2.course_list
    for course2 in courses_list_2:
        lu_crs_2.append(lookup_course_equivalence(course2))

    intersect = set(lu_crs_1).intersection(set(lu_crs_2))
    return len(intersect)

def lookup_course_equivalence(course_code: str):
    if course_code in course_equals['forward']:
        crs = course_equals['forward'][course_code][0]
    else:
        crs = course_code
    return crs

def refine_course_codes(requirement_courses: list):
    rtn_course_codes = list()
    for course in requirement_courses:
        if course is None:
            print("Found a None course")
            continue
        if 'original' in course:
            course_code = course['original']['code']
        elif 'canonical' in course:
            course_code = course['canonical']['code']
        else:
            print(f"*** skipping course, no original or canonical course data")
            continue
        course_code = lookup_course_equivalence(course_code)
        if course_code not in rtn_course_codes:
            rtn_course_codes.append(str(course_code))
    rtn_course_codes.sort()
    return rtn_course_codes

def get_hash(instring: str):
    concatenated_string = ''.join(instring)
    hash_object = hashlib.sha256(concatenated_string.encode())
    return hash_object.hexdigest()

def remove_space(course_list: list):
    rtn_list = list()
    # remove empty space in each course in the list
    for crs in course_list:
        rtn_list.append(''.join(crs.split()))
    return rtn_list

def calc_course_codes_hash(course_codes_list: list):
    clean_list = remove_space(course_codes_list)
    clean_list.sort()
    courses_str = ''
    for crs in clean_list:
        courses_str = courses_str + crs

    code_hash = get_hash(courses_str)
    return code_hash

academic_years = {
    116: 2023,
    112: 2022,
    108: 2021,
    104: 2020,
    100: 2019,
    96: 2018,
    92: 2017,
    88: 2016,
    84: 2015
}

def get_requirements(filter_program_code = None, intersect_threshold = 50):
    all_requirements = []
    matched_requirements = {}

    # step 2: loop thru each program or get specified one
    for course_data in layout_course_data:
        if "program" not in course_data:
            continue

        if course_data["type"] != "program":
            continue

        curr_program_code = course_data["program"]["code"]

        if (curr_program_code != filter_program_code) and (filter_program_code != None):
            continue

        # step 3: loop thru each academic period
        for period_data in course_data["academic_periods"]:
            if "MAJOR" not in period_data["degrees"]:
                continue

            if "plan_count" not in period_data or period_data["plan_count"] < 1:
                continue

            concentration_name = ""
            academic_year = academic_years[period_data['id']]
            academic_period = f'{str(academic_year)}80'

            if course_data["type"] == "concentration":
                concentration_name = course_data["concentration"]["code"]

            # step 4: pull requirements for program, academic period
            info = {
                'COURSE_PROGRAM_CODE': curr_program_code,
                'COURSE_CATALOG_ACADEMIC_PERIOD': academic_period,
                'COURSE_FIRST_MAJOR_CONC1': concentration_name
            }

            if (concentration_name != ""):
                continue

            program_data = get_program_data(info, 0)

            if program_data == None:
                print('No program data')
                continue

            try:
                # loop thru each requirement
                for requirement in program_data['requirements']:
                    requirement_courses = course_list_from_requirement(requirement, [])
                    # if requirement['type'] in ['CoreRequirement', 'CourseRequirement', 'DegreeRequirement']:
                    if requirement['type'] in ['DegreeRequirement']:
                        requirement_courses = course_list_from_requirement(requirement, [])
                        course_codes = refine_course_codes(requirement_courses)
                        req_course_hash = calc_course_codes_hash(course_codes)
                        if 'name' in requirement:
                            req_name = requirement['name']
                        else:
                            req_name = 'BLANK'
        
                        # step 5: build requirements object from retrieved data
                        requirement_id = str(requirement['id'])
                        requirements_data = {
                            "id": requirement_id,
                            "requirement_name": req_name,
                            "requirement_type": requirement['type'],
                            "program_code": curr_program_code,
                            "academic_period": academic_period,
                            "course_list": course_codes,
                            "courses_hash": req_course_hash,
                            "match_score": 0.0,
                            "name_match": False,
                            "group_id": str(uuid4())
                        }

                        requirement_instance = Requirements(**requirements_data)
                        all_requirements.append(requirement_instance)

            except ValidationError as e:
                print(f"Validation err: {e.json()}")
            except Exception as ex:
                print(f"Error: {ex}")

        # end loop thru academic period once requirements for all academic period are gathered
        # get just requirements for current program code
        program_requirements = [req for req in all_requirements if req.program_code == curr_program_code]
        sorted_program_requirements = sorted(
                program_requirements,
                key=lambda x: x.academic_period,
                reverse=True
            )

        to_csv(sorted_program_requirements, f"req_data/curr_requirements_{curr_program_code}.csv")
        
        # loop thru academic year again now that we have all requirements
        for curr_year in academic_years.values():
            curr_period = str(curr_year) + '80'

            prev_year = int(curr_year) - 1
            prev_period = str(prev_year) + '80'

            curr_requirements = [req for req in sorted_program_requirements if req.academic_period == curr_period]
            prev_requirements = [req for req in sorted_program_requirements if req.academic_period == prev_period]

            if len(curr_requirements) == 0 or len(prev_requirements) == 0:
                continue

            # step 6: score/compare requirements to previous academic period requirements, if there is one
            # compare each primary req based on program code to find similarities in second academic periods
            # use hash to find exact matches
            for curr_req in curr_requirements:
                for prev_req in prev_requirements:
                    intersect_count = intersection_count(curr_req, prev_req)
                    name_match = False
                    score, name_match = get_score(curr_req, prev_req, intersect_count)
                    # print(f'\tCurr Req: {curr_req.requirement_name}, Prev Req: {prev_req.requirement_name}, score: {score}')

                    # step 7: if intersect score is >= threshold, 
                    # set previous group_id = current group_id
                    if score >= intersect_threshold:
                        if name_match:
                            prev_req.group_id = curr_req.group_id
                        else:
                            if prev_req.name_match == False:
                                prev_req.group_id = curr_req.group_id

                        prev_req.name_match = name_match
                        prev_req.match_score = score
                        matched_requirements[curr_req.id] = curr_req
                        matched_requirements[prev_req.id] = prev_req

        # end loop academic years
    # end loop program
    return list(matched_requirements.values())

filter_program_code = None
# filter_program_code = 'BA-ANTH-AS'
# filter_program_code = 'BSME-ME-EN'
# filter_program_code = 'BA-AMST-AS'
# filter_program_code = 'BSCS-CS-EN'
# filter_program_code = 'BA-BIOC-AS'

# step 1: load data objects under static_data
f = open('./static_data/course_equivalence_latest.json')
course_equals = json.loads(f.read())
f.close()

f = open('./static_data/layouts.json')
layout_course_data = json.loads(f.read())
f.close()

requirements_list = get_requirements(filter_program_code, 70)
if filter_program_code:
    to_csv(requirements_list, f'req_data/matched_requirements_{filter_program_code}.csv')
else:
    to_csv(requirements_list, 'req_data/matched_requirements.csv')

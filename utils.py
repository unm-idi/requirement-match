from boto3.dynamodb.conditions import Key
import boto3
import json
import gzip
import base64
from urllib.parse import urlunsplit, urlencode
from requests import get

db = boto3.resource('dynamodb', 'us-west-2')

api_endpoint = 'api.nmrpsp.org'

__CohortRequirementDocumentsTable__ = "RequirementDocuments"
cohort_requirement_documents_table = db.Table(__CohortRequirementDocumentsTable__)

def course_list_from_requirement(requirement: dict, courses = []):
    """Returns a list of courses associated with the given requirement

    Args:
        requirement (dict): A CourseRequirement or DegreeRequirement
    """

    if requirement["type"] == "CourseRequirement":
        courses.append(requirement["course"])
    elif requirement["type"] == "CoreRequirement":
        for sub_requirement in requirement["core"]["subrequirements"]:
            course_list_from_requirement(sub_requirement, courses)
    elif requirement["type"] == "DegreeRequirement":
        for sub_requirement in requirement["subrequirements"]:
            course_list_from_requirement(sub_requirement, courses)
    # elif requirement["type"] == "WildCardRequirement":
    #     # Do something Here

    return courses
    


def decompress(zipped):
    return json.loads(gzip.decompress(base64.b64decode(zipped)))


def get_period_degree_join_id_requirements(id, total_credits=None, program_name=None, program_code=None):
    requirements = cohort_requirement_documents_table.query(
        IndexName='period-degree-join-document-id-index',
        KeyConditionExpression=Key('period_degree_join_document_id').eq(id)
    )['Items']

    pdj_data = {'requirements': [], 'id': id}
    if total_credits != None:
        pdj_data['total_credit_hours'] = total_credits

    if program_name != None:
        pdj_data['program_name'] = program_name

    if program_code != None:
        pdj_data['program_code'] = program_code

    for requirement in requirements:
        pdj_data['requirements'].append(decompress(base64.b64encode(requirement['requirement_info'].__str__())))

    return pdj_data


def get_period_degree_join_requirements(program_code, concentration_code = None, academic_year = None):
    # Load Static Crosswalk
    # print(os.path.dirname(os.path.realpath(__file__)))
    f = open('./static_data/determined_program_map.json')
    prog_corsswalk = json.load(f)
    f.close()

    year = academic_year.split('-')[0]

    # Determine Map
    mapped_code = program_code
    if program_code in prog_corsswalk[year] and prog_corsswalk[year][program_code] != '':
      mapped_code = prog_corsswalk[year][program_code]

    print("PROG CODE: " + mapped_code)

    pdj_domain = api_endpoint
    pdj_path = '/unm/period-degree-join/' + mapped_code
    pdj_query = {}
    if concentration_code:
        pdj_query['concentration_code'] = concentration_code
    if academic_year:
        pdj_query['academic_year'] = academic_year
    pdj_query = urlencode(pdj_query)
    pdj_url = urlunsplit(('https', pdj_domain, pdj_path, pdj_query, ""))
    print(pdj_url)
    pdj_data = get(pdj_url).json()
    return get_period_degree_join_id_requirements(int(pdj_data['id']), int(pdj_data['total_credit_hours']), pdj_data['program']['name'], pdj_data['program']['code'])


def get_period_slug(year):
    periods_to_slug = {2023: '2023-2024',
                       2022: '2022-2023',
                       2021: '2021-2022',
                       2020: '2020-2021',
                       2019: '2019-2020',
                       2018: '2018-2019',
                       2017: '2017-2018',
                       2016: '2016-2017',
                       2015: '2015-2016'}
    return periods_to_slug[year]


def get_program_data(info, audit_id):
    program_data = None

    program_name = info['COURSE_PROGRAM_CODE']
    concentration_name = ''
    catalog_period = info['COURSE_CATALOG_ACADEMIC_PERIOD']

    catalog_year = str(catalog_period)[0:4]
    catalog_slug = get_period_slug(int(catalog_year))
    
    try:
        if info['COURSE_FIRST_MAJOR_CONC1'] != '':
            concentration_name = info['COURSE_FIRST_MAJOR_CONC1']
            program_data = get_period_degree_join_requirements(
                program_name,
                concentration_code=concentration_name,
                academic_year=catalog_slug
            )
        else:
            program_data = get_period_degree_join_requirements(
                program_name,
                academic_year=catalog_slug
            )
    except:
        print('Error fetching program data')
    
    return program_data

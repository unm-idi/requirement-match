import json
import hashlib
import pandas as pd
from uuid import uuid4
from utils import get_program_data, course_list_from_requirement

def get_hash(instring):
    concatenated_string = ''.join(instring)
    hash_object = hashlib.sha256(concatenated_string.encode())
    return hash_object.hexdigest()

# def get_alt_courses(course_in: str):
#     return course_eq.str.startswith(course_in)

def remove_space(course_list: list):
    rtn_list = list()
    # remove empty space in each course in the list
    for crs in course_list:
        rtn_list.append(''.join(crs.split()))
    return rtn_list

def lookup_course_equivalence(course_code):
    if course_code in course_eq['forward']:
        crs = course_eq['forward'][course_code][0]
    else:
        crs = course_code
    return crs

def intersection_count(course_df_1, course_df_2):
    lu_crs_1 = list()
    lu_crs_2 = list()
    courses_list_1 = course_df_1['course_list']
    for course1 in courses_list_1:
        lu_crs_1.append(lookup_course_equivalence(course1))

    courses_list_2 = course_df_2['course_list']
    for course2 in courses_list_2:
        lu_crs_2.append(lookup_course_equivalence(course2))

    intersect = set(lu_crs_1).intersection(set(lu_crs_2))

    print(f"\Current year courses {len(lu_crs_1)}, previous year courses {len(lu_crs_2)}")
    print(f"\tIntersect count: {len(intersect)}")
    return len(intersect)

def similarity_score(len_prime, len_second, len_intersect):
    if len_prime == 0 or len_second == 0:
        return 0
    return ((len_intersect / len_prime) + (len_intersect / len_second)) / 2

def refine_course_codes(requirement_courses):
    rtn_course_code = list()
    for course in requirement_courses:
        if course is None:
            break
        if 'original' in course:
            course_code = course['original']['code']
        elif 'canonical' in course:
            course_code = course['canonical']['code']
        else:
            print(f"*** skipping course, no original or canonical course data")
            break
        course_code = lookup_course_equivalence(course_code)
        rtn_course_code.append(course_code)
    
    return rtn_course_code

def calc_course_codes_hash(course_codes_list: list):
    clean_list = remove_space(course_codes_list)
    clean_list.sort()
    courses_str = ''
    for crs in clean_list:
        courses_str = courses_str + crs

    code_hash = get_hash(courses_str)
    course_hash_str = str(code_hash)

    return course_hash_str


def find_canonical_reqs(filter_program_code = None):
    f = open('./static_data/layouts.json')
    bb_data = json.loads(f.read())
    f.close()

    intersect_threshold = 90

    out_columns = ['id', 'req_name', 'program', 'req_type', 'academic_period', 'course_list', 'hash', 'score', 'group_id']
    # all programs for each academic year with requirements and courses
    df_reqs = pd.DataFrame(columns=out_columns)
    df_total_out = pd.DataFrame(columns=out_columns)
    df_program_reqs = pd.DataFrame(columns=out_columns)

    for layout_data in bb_data:
        if "program" not in layout_data:
            continue

        program_code = layout_data["program"]["code"]

        if (program_code != filter_program_code) and (filter_program_code != None):
            continue

        for period_data in layout_data["academic_periods"]:
            if "MAJOR" not in period_data["degrees"]:
                continue

            if "plan_count" not in period_data or period_data["plan_count"] < 1:
                continue

            concentration_name = ""
            academic_year = layouts[period_data['id']]
            academic_period = f'{academic_year}80'

            if layout_data["type"] == "concentration":
                concentration_name = layout_data["concentration"]["code"]

            # Pull Requirements
            info = {
                'COURSE_PROGRAM_CODE': program_code,
                'COURSE_CATALOG_ACADEMIC_PERIOD': academic_period,
                'COURSE_FIRST_MAJOR_CONC1': concentration_name
            }

            if (concentration_name != ""):
                continue

            program_data = get_program_data(info, 0)

            if program_data == None:
                print('No program data')
                continue

            try:
                for requirement in program_data['requirements']:
                    # if requirement['type'] in ['CoreRequirement', 'CourseRequirement', 'DegreeRequirement']:
                    if requirement['type'] in ['DegreeRequirement']:
                        requirement_courses = course_list_from_requirement(requirement, [])
                        course_codes = refine_course_codes(requirement_courses)
                        # print(f"Required courses: {course_codes}")
                        req_course_hash = calc_course_codes_hash(course_codes)
                        if 'name' in requirement:
                            req_name = [requirement['name']]
                        else:
                            req_name = 'BLANK'
                        
                        # if requirement['type'] in ['DegreeRequirement']:
                        df_temp = pd.DataFrame({'id': [requirement['id']], 
                                                'req_name': [req_name], 
                                                'program': [program_code], 
                                                'req_type': [requirement['type']], 
                                                'academic_period': [academic_period], 
                                                'course_list': [course_codes], 
                                                'hash': [req_course_hash],
                                                'score': 0,
                                                'group_id': [uuid4()]
                                                })
                        df_reqs = pd.concat([df_reqs, df_temp], ignore_index = True).copy()

            except Exception as ex:
                print(f"Error: {ex}")

        df_reqs.sort_values(by=['program'], inplace=True, ignore_index=True)
        df_program_reqs = df_reqs[(df_reqs['program']==program_code)].sort_values(by=['academic_period'], ascending=False).copy()
        # df_program_reqs.drop_duplicates(subset=['id'])
        df_program_reqs.to_csv(f'./req_data/{program_code}.csv', encoding='utf-8')

        # df_program_reqs = df_program.sort_values(by=['academic_period', 'hash']).copy()

        df_out = pd.DataFrame(columns=out_columns)

        for prd1 in layouts.values():
            curr_period = prd1 + '80'

            prd2 = int(prd1) - 1
            prev_period = str(prd2) + '80'

            # make a list of all req's in current and previous academic period
            curr_df_reqs = df_program_reqs[(df_program_reqs['academic_period']==curr_period)].copy() # & (df_reqs['program']==program_code)]
            # curr_df_reqs = curr_df.sort_values(by='hash').copy()
            prev_df_reqs = df_program_reqs[(df_program_reqs['academic_period']==prev_period)].copy() # & (df_reqs['program']==program_code)]
            # prev_df_reqs = prev_df.sort_values(by='hash').copy()

            curr_req_count = curr_df_reqs.shape[0]
            prev_req_count = prev_df_reqs.shape[0]
                
            print(f"Number of reqs for {curr_period}: ", curr_req_count)
            print(f"Number of reqs for {prev_period}: ", prev_req_count)

            if curr_req_count == 0 or prev_req_count == 0:
                continue

            # compare each primary req based on program code to find similarities in second academic periods, can use hash for exact matches
            for curr_index, curr_row in curr_df_reqs.iterrows():
                for prev_index, prev_row in prev_df_reqs.iterrows():
                    print(f"{curr_row['req_name']} vs. {prev_row['req_name']}")

                    intersect_count = intersection_count(curr_row, prev_row)

                    if curr_row['req_name'] == prev_row['req_name']:
                        if len(curr_row['course_list']) > 0:
                            score = similarity_score(len(curr_row['course_list']), len(prev_row['course_list']), intersect_count)
                            score = round(score * 100, 2)
                        else:
                            score = 100
                    elif curr_row['hash'] == prev_row['hash'] and len(curr_row['course_list']) > 0:
                        score = 100
                    else:
                        score = similarity_score(len(curr_row['course_list']), len(prev_row['course_list']), intersect_count)
                        score = round(score * 100, 2)
                    
                    print(f"\tScore: {score}")
                    if score >= intersect_threshold:
                        group_id = curr_row['group_id']
                        df_program_reqs.loc[df_program_reqs['id'] == prev_row['id'], 'group_id'] = group_id

                        df_curr_out = pd.DataFrame({'id': [curr_row['id']], 
                                            'req_name': [curr_row['req_name']], 
                                            'program': [curr_row['program']], 
                                            'req_type': [curr_row['req_type']], 
                                            'academic_period': [curr_row['academic_period']], 
                                            'course_list': [curr_row['course_list']], 
                                            'hash': [curr_row['hash']],
                                            'score': [score],
                                            'group_id': [curr_row['group_id']]
                                                    })
                        
                        df_prev_out = pd.DataFrame({'id': [prev_row['id']],
                                                'req_name': [prev_row['req_name']], 
                                                'program': [prev_row['program']], 
                                                'req_type': [prev_row['req_type']], 
                                                'academic_period': [prev_row['academic_period']], 
                                                'course_list': [prev_row['course_list']], 
                                                'hash': [prev_row['hash']],
                                                'score': [score],
                                                'group_id': [curr_row['group_id']]
                        })

                        # set id_check of current academic year and get group id
                        id_check = set(df_curr_out['id'].values)

                        if not any(df_out['id'].isin(id_check)):
                            df_out = pd.concat([df_out, df_curr_out], ignore_index=True).copy()

                        # if not any(df_total_out['id'].isin(id_check)):
                        #     df_total_out = pd.concat([df_total_out, df_curr_out], ignore_index=True)
                        
                        # check id in previous academic year
                        id_check = set(df_prev_out['id'].values)
                        if not any(df_out['id'].isin(id_check)):
                            df_out = pd.concat([df_out, df_prev_out], ignore_index=True).copy()

                        # if not any(df_total_out['id'].isin(id_check)):
                        #     df_total_out = pd.concat([df_total_out, df_prev_out], ignore_index=True)
                        
                        # df_out.drop_duplicates(subset=['id'])

        print("\t**************************\n\n")
        # df_out.sort_values(by='req_name', ascending=False)
        df_total_out = pd.concat([df_total_out, df_out], ignore_index=True)
        df_out.to_csv(f'./req_data/{program_code}_requirements.csv', encoding='utf-8')


        # df_program_reqs.to_csv(f'./req_data/{program_code}_requirements.csv', encoding='utf-8')
        # df_out = pd.DataFrame(columns=columns_out)
        # df_reqs.to_csv(f'./req_data/{program_code}_requirements.csv', encoding='utf-8')
        # print(f"Total requirements in {df_reqs['program'][0]}: ", df_reqs.shape[0])
        # for prd1 in layouts.values():
        #     prime_period = prd1 + '80'
        #     second_period = prime_period - 1
        #     for prd2 in layouts.values():
        #         second_period = prd2 + '80'
        #         if prime_period == second_period:
        #             continue
        #         # make a list of all req's in primary academic period
        #         prime_df_reqs = df_program_reqs[(df_program_reqs['academic_period']==prime_period) & (df_reqs['program']==program_code)]
        #         second_df_reqs = df_program_reqs[(df_program_reqs['academic_period']==second_period) & (df_reqs['program']==program_code)]
        #         prime_req_count = prime_df_reqs.shape[0]
        #         second_req_count = second_df_reqs.shape[0]
        #         if prime_req_count == 0 or second_req_count == 0:
        #             continue
        #         print(f"Number of prime reqs for {prime_period}: ", prime_req_count)
        #         print(f"Number of second reqs for {second_period}: ", second_req_count)
        #         # compare each primary req based on program code to find similarities in second academic periods, can use hash for exact matches
        #         for prime_index, prime_row in prime_df_reqs.iterrows():
        #             for second_index, second_row in second_df_reqs.iterrows():
        #                 print(f"Prime {prime_row['req_name']}") # id: {prime_row['id']}")
        #                 print(f"Second {second_row['req_name']}") # id: {second_row['id']}")
        #                 intersect_count = intersection_count(prime_row, second_row)
        #                 # if intersect_count > 0:
        #                 if prime_row['hash'] == second_row['hash']:
        #                     score = 100
        #                 else:
        #                     score = similarity_score(len(prime_row['course_list']), len(second_row['course_list']), intersect_count)
        #                     score = round(score * 100, 2) 
        #                 print(f"\tIntersection score: {score} %")
        #                 if score > intersect_threshold:
        #                     df_temp_out = pd.DataFrame({'program_code': [program_code],
        #                                                 'period_1': [prime_period],
        #                                                 'req_id_1': [prime_row['id']],
        #                                                 'req_name_1': [prime_row['req_name']],
        #                                                 'course_list_1': [prime_row['course_list']],
        #                                                 'period_2': [second_period],
        #                                                 'req_id_2': [second_row['id']],
        #                                                 'req_name_2': [second_row['req_name']],
        #                                                 'course_list_2': [second_row['course_list']],
        #                                                 'score': [score]
        #                                                 })
        #                     df_out = pd.concat([df_out, df_temp_out], ignore_index = True)
        #                     df_total_out = pd.concat([df_total_out, df_out], ignore_index = True)
        #                 print("\t**************************\n\n")

    return df_total_out.drop_duplicates(subset=['id'])

# Begin
layouts = {
    116: '2023',
    112: '2022',
    108: '2021',
    104: '2020',
    100: '2019',
    96: '2018',
    92: '2017',
    88: '2016',
    84: '2015'
}

# get program code and academic period to find matches
# filter_program_code = 'BA-ANTH-AS'
# filter_program_code = 'BSME-ME-EN'
filter_program_code = 'BA-AMST-AS'
# filter_program_code = None


f = open('./static_data/course_equivalence_latest.json')
course_eq = json.loads(f.read())
f.close()

df_out = find_canonical_reqs(filter_program_code)
df_out.to_csv('./req_data/requirements_matching.csv', encoding='utf-8')